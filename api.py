# -*- coding: utf-8 -*-
#%% Libraries
import os
import pandas as pd
import uvicorn
from fastapi import FastAPI
from prophet import Prophet
from prophet.serialize import model_from_json
from pydantic import BaseModel

#%% API Setup
# Assign an instance of the FastAPI class to the variable "app"
app = FastAPI(title='Deploying a Forecasting Model with FastAPI')

# Load the available forecasting model.
def load_model():
    with open('model.json', 'r') as fin:
        model = model_from_json(fin.read())  
    return model 

# Define Input class for model inputs.
class Inputs(BaseModel):
    periods: int
    freq: str      

# By using @app.get("/") we are allowing the GET method to work for the / endpoint.
@app.get("/")
def home():
    return {"message": "TICKIT Dataset Forecasting"}

# This endpoint handles all the logic necessary for the forecasting model to work.
@app.post("/predict") 
async def prediction(inputs: Inputs):
    # periods: integer for the number of periods
    # freq: string 'D' for days, 'W' for weeks, 'M' for months.
    model = load_model()
    df_future = model.make_future_dataframe(periods=inputs.periods, freq=inputs.freq)
    forecast_prophet = model.predict(df_future)
    
    df_sales_future = forecast_prophet[['ds','yhat']]
    df_sales_future['year'] = df_sales_future['ds'].dt.year
    df_sales_future = df_sales_future.query('year == 2009')
    
    df_sales_future['date'] = df_sales_future['ds'].dt.strftime('%Y-%m-%d')
    df_sales_future['yhat'] = df_sales_future['yhat'].astype('int')
    
    response = df_sales_future[['date','yhat']].to_json(orient='index')
    
    return response
